document.addEventListener("DOMContentLoaded", documentDOMContentLoaded);

var dom = document;
var dy = 0;
var encabezado;
var elementosDevelar;
var elementosMover;


function documentDOMContentLoaded( evt ){
	dom.querySelector(".hamburguesa").addEventListener( "click", hamburguesaClick );
	dom.querySelector(".menu .cerrar").addEventListener( "click", menuCerrarClick );

	var enlaces = dom.querySelectorAll( "a" );

	enlaces.forEach( function( elem ){
		elem.addEventListener( "click", function( evt ){
			var id = this.attributes.href.value;
			var objetivo = dom.querySelector(id);
			if ( objetivo ) {
				TweenLite.to(window, 0.5, {scrollTo: id, ease: Power1.easeInOut });
				//window.scrollTo( 0, 1900);
				dom.querySelector( "#navegacion" ).classList.remove( "abierto" );
				evt.preventDefault();
			}
		});
	});

	encabezado = dom.getElementById( "encabezado" );
	elementosDevelar = dom.querySelectorAll( "[data-develar]" );
	elementosMover = dom.querySelectorAll( "[data-mover]" );
	develar();

	window.addEventListener( "scroll", windowScroll );
	requestAnimationFrame( animacionCuadro );
}

function hamburguesaClick( evt ) {
	dom.querySelector( "#navegacion" ).classList.add( "abierto" );
}

function menuCerrarClick( evt ) {
	dom.querySelector( "#navegacion" ).classList.remove( "abierto" );
}


function windowScroll( evt ) {
	//dy = window.scrollY;
}

function animacionCuadro( timestamp ) {

	if( dy != window.scrollY ){
		dy = window.scrollY;

		if ( dy > 1 ) {
			encabezado.classList.add( "anclado" );
		}
		else{
			encabezado.classList.remove( "anclado" );
		}

		develar();
	}
	mover();


	requestAnimationFrame(animacionCuadro);
}

function develar(){

	elementosDevelar.forEach( function( elem ){
		//var py = 0;
		//var ancestro = elem.offsetParent;
		/*
		do {
			if(!isNaN(ancestro.offsetTop)) {
				py += ancestro.offsetTop;
			}
		} while( ancestro = ancestro.offsetParent )
		py += elem.offsetTop;

		if(py >= sy && py <= sy + area){
			elem.classList.add( "aparece" );
			elem.classList.remove( "desaparece" );
		} else{
			elem.classList.remove( "aparece" );
			elem.classList.add( "desaparece" );
		}*/
		if ( esVisible( elem )) {
			elem.classList.add( "aparece" );
			elem.classList.remove( "desaparece" );
		} else{
			elem.classList.remove( "aparece" );
			elem.classList.add( "desaparece" );
		}
	});
}

function mover(){
	elementosMover.forEach( function( elem ){
		var py = coordenadaRelativa( elem )
		//console.debug( elem.className, py );
		var ds = elem.dataset.mover / 2;
		var ry = py - 0.5;
		//elem.style.opacity = py;
		elem.style.transform = "translate(0,"+( ds*ry )+"px)";
		//elem.style.
		//TweenLite.to( elem, 0.2, { y: ( ds*ry ) });
	});
}

function esVisible( elemento ){
	var sy = window.pageYOffset;
	var area = window.innerHeight;

	var py = 0;
	var ancestro = elemento.offsetParent;
	do {
		if(!isNaN(ancestro.offsetTop)) {
			py += ancestro.offsetTop;
		}
	} while( ancestro = ancestro.offsetParent )
	py += elemento.offsetTop;
	ms = elemento.offsetHeight * 0.7;
	if( py > sy - ms && py < sy + area){
		return true;
	}
	else{
		return false;
	}
}

function coordenadaRelativa( elemento ){
	var sy = window.pageYOffset;
	var area = window.innerHeight;

	var py = 0;
	var ancestro = elemento.offsetParent;
	do {
		if(!isNaN(ancestro.offsetTop)) {
			py += ancestro.offsetTop;
		}
	} while( ancestro = ancestro.offsetParent )
	py += elemento.offsetTop;
	if(py >= sy && py <= sy + area){
		return ((py + elemento.offsetHeight)- sy) / area;
	}
	else{
		return false;
	}
}

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJnZW5lcmFsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGRvY3VtZW50RE9NQ29udGVudExvYWRlZCk7XHJcblxyXG52YXIgZG9tID0gZG9jdW1lbnQ7XHJcbnZhciBkeSA9IDA7XHJcbnZhciBlbmNhYmV6YWRvO1xyXG52YXIgZWxlbWVudG9zRGV2ZWxhcjtcclxudmFyIGVsZW1lbnRvc01vdmVyO1xyXG5cclxuXHJcbmZ1bmN0aW9uIGRvY3VtZW50RE9NQ29udGVudExvYWRlZCggZXZ0ICl7XHJcblx0ZG9tLnF1ZXJ5U2VsZWN0b3IoXCIuaGFtYnVyZ3Vlc2FcIikuYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCBoYW1idXJndWVzYUNsaWNrICk7XHJcblx0ZG9tLnF1ZXJ5U2VsZWN0b3IoXCIubWVudSAuY2VycmFyXCIpLmFkZEV2ZW50TGlzdGVuZXIoIFwiY2xpY2tcIiwgbWVudUNlcnJhckNsaWNrICk7XHJcblxyXG5cdHZhciBlbmxhY2VzID0gZG9tLnF1ZXJ5U2VsZWN0b3JBbGwoIFwiYVwiICk7XHJcblxyXG5cdGVubGFjZXMuZm9yRWFjaCggZnVuY3Rpb24oIGVsZW0gKXtcclxuXHRcdGVsZW0uYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCBmdW5jdGlvbiggZXZ0ICl7XHJcblx0XHRcdHZhciBpZCA9IHRoaXMuYXR0cmlidXRlcy5ocmVmLnZhbHVlO1xyXG5cdFx0XHR2YXIgb2JqZXRpdm8gPSBkb20ucXVlcnlTZWxlY3RvcihpZCk7XHJcblx0XHRcdGlmICggb2JqZXRpdm8gKSB7XHJcblx0XHRcdFx0VHdlZW5MaXRlLnRvKHdpbmRvdywgMC41LCB7c2Nyb2xsVG86IGlkLCBlYXNlOiBQb3dlcjEuZWFzZUluT3V0IH0pO1xyXG5cdFx0XHRcdC8vd2luZG93LnNjcm9sbFRvKCAwLCAxOTAwKTtcclxuXHRcdFx0XHRkb20ucXVlcnlTZWxlY3RvciggXCIjbmF2ZWdhY2lvblwiICkuY2xhc3NMaXN0LnJlbW92ZSggXCJhYmllcnRvXCIgKTtcclxuXHRcdFx0XHRldnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fSk7XHJcblxyXG5cdGVuY2FiZXphZG8gPSBkb20uZ2V0RWxlbWVudEJ5SWQoIFwiZW5jYWJlemFkb1wiICk7XHJcblx0ZWxlbWVudG9zRGV2ZWxhciA9IGRvbS5xdWVyeVNlbGVjdG9yQWxsKCBcIltkYXRhLWRldmVsYXJdXCIgKTtcclxuXHRlbGVtZW50b3NNb3ZlciA9IGRvbS5xdWVyeVNlbGVjdG9yQWxsKCBcIltkYXRhLW1vdmVyXVwiICk7XHJcblx0ZGV2ZWxhcigpO1xyXG5cclxuXHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggXCJzY3JvbGxcIiwgd2luZG93U2Nyb2xsICk7XHJcblx0cmVxdWVzdEFuaW1hdGlvbkZyYW1lKCBhbmltYWNpb25DdWFkcm8gKTtcclxufVxyXG5cclxuZnVuY3Rpb24gaGFtYnVyZ3Vlc2FDbGljayggZXZ0ICkge1xyXG5cdGRvbS5xdWVyeVNlbGVjdG9yKCBcIiNuYXZlZ2FjaW9uXCIgKS5jbGFzc0xpc3QuYWRkKCBcImFiaWVydG9cIiApO1xyXG59XHJcblxyXG5mdW5jdGlvbiBtZW51Q2VycmFyQ2xpY2soIGV2dCApIHtcclxuXHRkb20ucXVlcnlTZWxlY3RvciggXCIjbmF2ZWdhY2lvblwiICkuY2xhc3NMaXN0LnJlbW92ZSggXCJhYmllcnRvXCIgKTtcclxufVxyXG5cclxuXHJcbmZ1bmN0aW9uIHdpbmRvd1Njcm9sbCggZXZ0ICkge1xyXG5cdC8vZHkgPSB3aW5kb3cuc2Nyb2xsWTtcclxufVxyXG5cclxuZnVuY3Rpb24gYW5pbWFjaW9uQ3VhZHJvKCB0aW1lc3RhbXAgKSB7XHJcblxyXG5cdGlmKCBkeSAhPSB3aW5kb3cuc2Nyb2xsWSApe1xyXG5cdFx0ZHkgPSB3aW5kb3cuc2Nyb2xsWTtcclxuXHJcblx0XHRpZiAoIGR5ID4gMSApIHtcclxuXHRcdFx0ZW5jYWJlemFkby5jbGFzc0xpc3QuYWRkKCBcImFuY2xhZG9cIiApO1xyXG5cdFx0fVxyXG5cdFx0ZWxzZXtcclxuXHRcdFx0ZW5jYWJlemFkby5jbGFzc0xpc3QucmVtb3ZlKCBcImFuY2xhZG9cIiApO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRldmVsYXIoKTtcclxuXHR9XHJcblx0bW92ZXIoKTtcclxuXHJcblxyXG5cdHJlcXVlc3RBbmltYXRpb25GcmFtZShhbmltYWNpb25DdWFkcm8pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBkZXZlbGFyKCl7XHJcblxyXG5cdGVsZW1lbnRvc0RldmVsYXIuZm9yRWFjaCggZnVuY3Rpb24oIGVsZW0gKXtcclxuXHRcdC8vdmFyIHB5ID0gMDtcclxuXHRcdC8vdmFyIGFuY2VzdHJvID0gZWxlbS5vZmZzZXRQYXJlbnQ7XHJcblx0XHQvKlxyXG5cdFx0ZG8ge1xyXG5cdFx0XHRpZighaXNOYU4oYW5jZXN0cm8ub2Zmc2V0VG9wKSkge1xyXG5cdFx0XHRcdHB5ICs9IGFuY2VzdHJvLm9mZnNldFRvcDtcclxuXHRcdFx0fVxyXG5cdFx0fSB3aGlsZSggYW5jZXN0cm8gPSBhbmNlc3Ryby5vZmZzZXRQYXJlbnQgKVxyXG5cdFx0cHkgKz0gZWxlbS5vZmZzZXRUb3A7XHJcblxyXG5cdFx0aWYocHkgPj0gc3kgJiYgcHkgPD0gc3kgKyBhcmVhKXtcclxuXHRcdFx0ZWxlbS5jbGFzc0xpc3QuYWRkKCBcImFwYXJlY2VcIiApO1xyXG5cdFx0XHRlbGVtLmNsYXNzTGlzdC5yZW1vdmUoIFwiZGVzYXBhcmVjZVwiICk7XHJcblx0XHR9IGVsc2V7XHJcblx0XHRcdGVsZW0uY2xhc3NMaXN0LnJlbW92ZSggXCJhcGFyZWNlXCIgKTtcclxuXHRcdFx0ZWxlbS5jbGFzc0xpc3QuYWRkKCBcImRlc2FwYXJlY2VcIiApO1xyXG5cdFx0fSovXHJcblx0XHRpZiAoIGVzVmlzaWJsZSggZWxlbSApKSB7XHJcblx0XHRcdGVsZW0uY2xhc3NMaXN0LmFkZCggXCJhcGFyZWNlXCIgKTtcclxuXHRcdFx0ZWxlbS5jbGFzc0xpc3QucmVtb3ZlKCBcImRlc2FwYXJlY2VcIiApO1xyXG5cdFx0fSBlbHNle1xyXG5cdFx0XHRlbGVtLmNsYXNzTGlzdC5yZW1vdmUoIFwiYXBhcmVjZVwiICk7XHJcblx0XHRcdGVsZW0uY2xhc3NMaXN0LmFkZCggXCJkZXNhcGFyZWNlXCIgKTtcclxuXHRcdH1cclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gbW92ZXIoKXtcclxuXHRlbGVtZW50b3NNb3Zlci5mb3JFYWNoKCBmdW5jdGlvbiggZWxlbSApe1xyXG5cdFx0dmFyIHB5ID0gY29vcmRlbmFkYVJlbGF0aXZhKCBlbGVtIClcclxuXHRcdC8vY29uc29sZS5kZWJ1ZyggZWxlbS5jbGFzc05hbWUsIHB5ICk7XHJcblx0XHR2YXIgZHMgPSBlbGVtLmRhdGFzZXQubW92ZXIgLyAyO1xyXG5cdFx0dmFyIHJ5ID0gcHkgLSAwLjU7XHJcblx0XHQvL2VsZW0uc3R5bGUub3BhY2l0eSA9IHB5O1xyXG5cdFx0ZWxlbS5zdHlsZS50cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZSgwLFwiKyggZHMqcnkgKStcInB4KVwiO1xyXG5cdFx0Ly9lbGVtLnN0eWxlLlxyXG5cdFx0Ly9Ud2VlbkxpdGUudG8oIGVsZW0sIDAuMiwgeyB5OiAoIGRzKnJ5ICkgfSk7XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGVzVmlzaWJsZSggZWxlbWVudG8gKXtcclxuXHR2YXIgc3kgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblx0dmFyIGFyZWEgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XHJcblxyXG5cdHZhciBweSA9IDA7XHJcblx0dmFyIGFuY2VzdHJvID0gZWxlbWVudG8ub2Zmc2V0UGFyZW50O1xyXG5cdGRvIHtcclxuXHRcdGlmKCFpc05hTihhbmNlc3Ryby5vZmZzZXRUb3ApKSB7XHJcblx0XHRcdHB5ICs9IGFuY2VzdHJvLm9mZnNldFRvcDtcclxuXHRcdH1cclxuXHR9IHdoaWxlKCBhbmNlc3RybyA9IGFuY2VzdHJvLm9mZnNldFBhcmVudCApXHJcblx0cHkgKz0gZWxlbWVudG8ub2Zmc2V0VG9wO1xyXG5cdG1zID0gZWxlbWVudG8ub2Zmc2V0SGVpZ2h0ICogMC43O1xyXG5cdGlmKCBweSA+IHN5IC0gbXMgJiYgcHkgPCBzeSArIGFyZWEpe1xyXG5cdFx0cmV0dXJuIHRydWU7XHJcblx0fVxyXG5cdGVsc2V7XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG59XHJcblxyXG5mdW5jdGlvbiBjb29yZGVuYWRhUmVsYXRpdmEoIGVsZW1lbnRvICl7XHJcblx0dmFyIHN5ID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG5cdHZhciBhcmVhID0gd2luZG93LmlubmVySGVpZ2h0O1xyXG5cclxuXHR2YXIgcHkgPSAwO1xyXG5cdHZhciBhbmNlc3RybyA9IGVsZW1lbnRvLm9mZnNldFBhcmVudDtcclxuXHRkbyB7XHJcblx0XHRpZighaXNOYU4oYW5jZXN0cm8ub2Zmc2V0VG9wKSkge1xyXG5cdFx0XHRweSArPSBhbmNlc3Ryby5vZmZzZXRUb3A7XHJcblx0XHR9XHJcblx0fSB3aGlsZSggYW5jZXN0cm8gPSBhbmNlc3Ryby5vZmZzZXRQYXJlbnQgKVxyXG5cdHB5ICs9IGVsZW1lbnRvLm9mZnNldFRvcDtcclxuXHRpZihweSA+PSBzeSAmJiBweSA8PSBzeSArIGFyZWEpe1xyXG5cdFx0cmV0dXJuICgocHkgKyBlbGVtZW50by5vZmZzZXRIZWlnaHQpLSBzeSkgLyBhcmVhO1xyXG5cdH1cclxuXHRlbHNle1xyXG5cdFx0cmV0dXJuIGZhbHNlO1xyXG5cdH1cclxufVxyXG4iXSwiZmlsZSI6ImdlbmVyYWwuanMifQ==
