document.addEventListener("DOMContentLoaded", documentDOMContentLoaded);

var dom = document;
var dy = 0;
var encabezado;
var elementosDevelar;
var elementosMover;


function documentDOMContentLoaded( evt ){
	dom.querySelector(".hamburguesa").addEventListener( "click", hamburguesaClick );
	dom.querySelector(".menu .cerrar").addEventListener( "click", menuCerrarClick );

	var enlaces = dom.querySelectorAll( "a" );

	enlaces.forEach( function( elem ){
		elem.addEventListener( "click", function( evt ){
			var id = this.attributes.href.value;
			var objetivo = dom.querySelector(id);
			if ( objetivo ) {
				TweenLite.to(window, 0.5, {scrollTo: id, ease: Power1.easeInOut });
				//window.scrollTo( 0, 1900);
				dom.querySelector( "#navegacion" ).classList.remove( "abierto" );
				evt.preventDefault();
			}
		});
	});

	encabezado = dom.getElementById( "encabezado" );
	elementosDevelar = dom.querySelectorAll( "[data-develar]" );
	elementosMover = dom.querySelectorAll( "[data-mover]" );
	develar();

	window.addEventListener( "scroll", windowScroll );
	requestAnimationFrame( animacionCuadro );
}

function hamburguesaClick( evt ) {
	dom.querySelector( "#navegacion" ).classList.add( "abierto" );
}

function menuCerrarClick( evt ) {
	dom.querySelector( "#navegacion" ).classList.remove( "abierto" );
}


function windowScroll( evt ) {
	//dy = window.scrollY;
}

function animacionCuadro( timestamp ) {

	if( dy != window.scrollY ){
		dy = window.scrollY;

		if ( dy > 1 ) {
			encabezado.classList.add( "anclado" );
		}
		else{
			encabezado.classList.remove( "anclado" );
		}

		develar();
	}
	mover();


	requestAnimationFrame(animacionCuadro);
}

function develar(){

	elementosDevelar.forEach( function( elem ){
		//var py = 0;
		//var ancestro = elem.offsetParent;
		/*
		do {
			if(!isNaN(ancestro.offsetTop)) {
				py += ancestro.offsetTop;
			}
		} while( ancestro = ancestro.offsetParent )
		py += elem.offsetTop;

		if(py >= sy && py <= sy + area){
			elem.classList.add( "aparece" );
			elem.classList.remove( "desaparece" );
		} else{
			elem.classList.remove( "aparece" );
			elem.classList.add( "desaparece" );
		}*/
		if ( esVisible( elem )) {
			elem.classList.add( "aparece" );
			elem.classList.remove( "desaparece" );
		} else{
			elem.classList.remove( "aparece" );
			elem.classList.add( "desaparece" );
		}
	});
}

function mover(){
	elementosMover.forEach( function( elem ){
		var py = coordenadaRelativa( elem )
		//console.debug( elem.className, py );
		var ds = elem.dataset.mover / 2;
		var ry = py - 0.5;
		//elem.style.opacity = py;
		elem.style.transform = "translate(0,"+( ds*ry )+"px)";
		//elem.style.
		//TweenLite.to( elem, 0.2, { y: ( ds*ry ) });
	});
}

function esVisible( elemento ){
	var sy = window.pageYOffset;
	var area = window.innerHeight;

	var py = 0;
	var ancestro = elemento.offsetParent;
	do {
		if(!isNaN(ancestro.offsetTop)) {
			py += ancestro.offsetTop;
		}
	} while( ancestro = ancestro.offsetParent )
	py += elemento.offsetTop;
	ms = elemento.offsetHeight * 0.7;
	if( py > sy - ms && py < sy + area){
		return true;
	}
	else{
		return false;
	}
}

function coordenadaRelativa( elemento ){
	var sy = window.pageYOffset;
	var area = window.innerHeight;

	var py = 0;
	var ancestro = elemento.offsetParent;
	do {
		if(!isNaN(ancestro.offsetTop)) {
			py += ancestro.offsetTop;
		}
	} while( ancestro = ancestro.offsetParent )
	py += elemento.offsetTop;
	if(py >= sy && py <= sy + area){
		return ((py + elemento.offsetHeight)- sy) / area;
	}
	else{
		return false;
	}
}
